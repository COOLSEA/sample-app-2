class User < ActiveRecord::Base

    has_many :microposts, dependent: :destroy

	before_save { self.email = email.downcase }
	before_create :create_remember_token             #用戶創建前，產生sessions『記憶標籤』

	validates :name, presence: true , length: { maximum: 50}
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true, 
    							format: { with: VALID_EMAIL_REGEX }, 
    							uniqueness: { case_sensitive: false }

    validates :password, length: { minimum: 6 }      #密碼要大於六位數

    has_secure_password                              #使用rails安全性設定


    def User.new_remember_token                      #使用base64隨身產生記憶標簽內容
    	SecureRandom.urlsafe_base64
    end

    def User.encrypt(token)							 #使用SHA1加密
    	Digest::SHA1.hexdigest(token.to_s)
    end

    def feed
        Micropost.where("user_id = ?",id)
    end


    private

    	def create_remember_token
    		self.remember_token = User.encrypt(User.new_remember_token)
    	end


end
